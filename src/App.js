import React, {Component} from "react";
import Button from "./components/Button";
import Modal from "./components/Modal/Modal";
import './style/style.scss';

class App extends Component {

  state = {
    firstModalIsOpen: false,
    secondModalIsOpen: false,
  }

  modalHandler = (e) => {
    const {firstModalIsOpen, secondModalIsOpen} = this.state;
    switch (+e.target.dataset.id) {
      case 1:
        this.setState({firstModalIsOpen: !firstModalIsOpen})
        break;
      case 2:
        this.setState({secondModalIsOpen: !secondModalIsOpen})
        break;
      default:
        return;
    }
  }

  render() {
    const {firstModalIsOpen, secondModalIsOpen} = this.state;
    const firstModal = <Modal header={"Do you want to delete this file?"}
                              closeButton={true}
                              dataId={1}
                              closeModal={this.modalHandler}
                              text={"Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?"}
                              actions={
                                <>
                                  <Button backgroundColor={"#b3382c"}
                                          text={"Ok"}
                                          dataId={1}
                                          clickHandler={this.modalHandler}/>
                                  <Button backgroundColor={"#b3382c"}
                                          text={"Cancel"}
                                          dataId={1}
                                          clickHandler={this.modalHandler}/>
                                </>
                              }
    />

    const secondModal = <Modal header={"Are you sure you want to delete this file?"}
                               closeButton={false}
                               dataId={2}
                               closeModal={this.modalHandler}
                               text={"This item will be deleted immediately. You can't undo this action"}
                               actions={
                                 <>
                                   <Button backgroundColor={"#000"}
                                           text={"Confirm"}
                                           dataId={2}
                                           clickHandler={this.modalHandler}/>
                                   <Button backgroundColor={"#000"}
                                           text={"Undo"}
                                           dataId={2}
                                           clickHandler={this.modalHandler}/>
                                 </>
                               }
    />


    return (
        <div className="App">
          <Button backgroundColor={"green"}
                  text="Open first modal"
                  clickHandler={this.modalHandler}
                  dataId={1}
          />
          <Button backgroundColor={"red"}
                  text="Open second modal"
                  clickHandler={this.modalHandler}
                  dataId={2}
          />
          {firstModalIsOpen && firstModal}
          {secondModalIsOpen && secondModal}
        </div>
    );
  }
}

export default App;
