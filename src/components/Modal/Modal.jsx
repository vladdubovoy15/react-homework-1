import React, {Component} from 'react';
import classes from './modal.module.scss';
import '../../style/style.scss'

class Modal extends Component {
  render() {
    const {header, text, actions, closeButton, closeModal, dataId} = this.props;

    return (
        <div className={classes["modal-overlay"]} data-id={dataId} onClick={(e) => closeModal(e)}>
          <div className={classes.wrapper} onClick={(e) => e.stopPropagation()}>
            <header className={classes.header}>
              <h2 className={classes.title}>{header}</h2>
              {closeButton && <button className={classes["close-button"]} data-id={dataId} onClick={(e) => closeModal(e)}>X</button>}
            </header>
            <p className={classes.text}>{text}</p>
            <div className={classes["action-wrapper"]}>
              {actions}
            </div>
          </div>
        </div>
    );
  }
}

export default Modal;